//
//  FEUserSession.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Foundation

//MARK: User Session
public struct FEUserSession: Codable {
    let accessToken: String?
    let refreshToken: String?
    
    static func saveLoginSession(session: Any) {
        if let encodedSession = try? JSONSerialization.data(withJSONObject: session) {
            let userDefaults = UserDefaults.standard
            userDefaults.set(encodedSession, forKey: "FEUserSession")
        }
    }

    static func getSession() -> FEUserSession? {
        let userDefaults = UserDefaults.standard
        guard let savedSessionData = userDefaults.object(forKey: "FEUserSession") as? Data,
              let savedSession = try? JSONDecoder().decode(FEUserSession.self, from: savedSessionData) else { return nil }
        return savedSession
    }
}

//MARK: Refresh Token
public struct FERefreshTokenRequest: Codable {
    let refreshToken: String?
    
    init(token: String?) {
        refreshToken = token
    }
}


public struct FEAuthLoginRequest: Codable { 
    let apiKey: String
    var userIdentifier: String = ""
}

public struct FEUserRegisterRequest: Codable {
    let apiKey: String
    var userIdentifier: String?
    var name: String?
}

public struct FEUpdateUserRequest: Codable {
    let name: String?
    var email: String?
    var phone: String?
}
public struct FEUpgradePlanRequest: Codable {
    let subscriptionId: String
}

public struct FEPublishActivityRequest: Codable {
    let activityCode: String
    let meta: Data?
}


