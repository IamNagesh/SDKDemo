//
//  FEExtensions.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Foundation

public protocol FLServiceInteractorProtocol {
    func request(forAPI router: APIRouter, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler)
}

extension Encodable {
    
    func convertToDictionary() throws -> [String: Any] {
        let data:Data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

extension Dictionary {
    func convertToData() -> Data? {
        do {
            let dictData = try JSONSerialization.data(withJSONObject: self)
            return dictData
        } catch {
            return nil
        }
    }
}

extension String {
    mutating func replaceURLParameters(withParameters parameters:[URLReplaceIdentifier : String]) {
        for (key,value) in parameters { self = self.replacingOccurrences(of: key.rawValue, with: value) }
    }
}
