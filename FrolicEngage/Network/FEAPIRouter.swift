//
//  FEAPIRouter.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Foundation
import Alamofire
public enum APIRouter: APIConfiguration {
    
   // case login(_ request: String)
    case authLogin(_ apiKey: String, _ userId: String)
    case userRegister(_ name: String, _ identifire: String, apiKey: String)
    case updateUserDetail(_ name: String?, _ email: String?, phone: String?)
    case refreshToken(_ request: FERefreshTokenRequest)
    case userSubscriptionDetail
    case subscriptionFeature(_ urlReplace: [URLReplaceIdentifier : String])
    case userSubscriptionFeature
    case renewSubscription
    case upgradePlan(_ subscriptionID: String)
    case userDetail
    case userActivityDetail
    case publishUserActivity(_ activityCode: String, meta: [String: Any])
    case getUserAllPreviousActivity

    static var baseURLWithVersion: URL { FEAppConfig.shared.apiBaseUrl.appendingPathComponent(version) }
    
    
    
    // MARK: - HTTPMethod
    var method: HTTPMethod {
        switch self {
        case .userRegister, .refreshToken, .authLogin, .upgradePlan:
            return .post
        case .userSubscriptionDetail, .subscriptionFeature, .userSubscriptionFeature, .renewSubscription, .userDetail:
            return .get
        case .userActivityDetail, .publishUserActivity, .getUserAllPreviousActivity:
            return .get
        case .updateUserDetail:
            return .patch
      
        }
    }
    
    
    // MARK: - Parameters
    var parameters: RequestParams {
        switch self {
        case .userRegister(let name, let userId,  let apiKey):
            let request = FEUserRegisterRequest(apiKey: apiKey, userIdentifier: userId, name: name)
            return .requestBody(request)
        case .updateUserDetail(let name, let email,  let phone):
            let request = FEUpdateUserRequest(name: name, email: email, phone: phone)
            return .requestBody(request)
        case .refreshToken(let request):
            return .requestBody(request)
        case .authLogin(let appkey, let userId):
            let request = FEAuthLoginRequest(apiKey: appkey, userIdentifier: userId)
            return .requestBody(request)
        case .userSubscriptionDetail:
            return .none
        case .subscriptionFeature(let urlReplace):
            return .urlReplace(urlReplace)
        case .userSubscriptionFeature:
            return .none
        case .renewSubscription:
            return .none
        case .upgradePlan(let subId):
            let request = FEUpgradePlanRequest(subscriptionId: subId)
            return .requestBody(request)
        case .userDetail:
            return .none
        case .userActivityDetail:
              return .none
        case .publishUserActivity(let activityCode, let meta):
            let metaData = meta.convertToData()
            let request = FEPublishActivityRequest(activityCode: activityCode, meta: metaData)
            return .requestBody(request)
        case .getUserAllPreviousActivity:
            return .none
        }
    }
    
    // MARK: - Path
    var path: String {
        switch self {
            // force update
        case .refreshToken:
            return FEAppConfig.shared.refreshAccesToken
        case .authLogin:
            return FEAppConfig.shared.getUserAccesToken
        case .userRegister:
            return FEAppConfig.shared.userRegister

        case .userSubscriptionDetail:
            return FEAppConfig.shared.getUserSubscriptionDetail
            
        case .subscriptionFeature:
            return FEAppConfig.shared.getSubscriptionFeature

        case .userSubscriptionFeature:
            return FEAppConfig.shared.getUserSubscriptionFeature

        case .renewSubscription:
            return FEAppConfig.shared.renewUserSubscription

        case .upgradePlan:
            return FEAppConfig.shared.upgradeUserSubscriptionPlan

        case .userDetail:
            return FEAppConfig.shared.userDetail
        case .updateUserDetail:
            return FEAppConfig.shared.userDetail

        case .userActivityDetail:
            return FEAppConfig.shared.getuserActivityDetail

        case .publishUserActivity:
            return FEAppConfig.shared.publishuserActivity

        case .getUserAllPreviousActivity:
            return FEAppConfig.shared.getUserAllPreviousActivity

        }
    }
    
    //MARK: Version
    static var version: String { return "v1/" }
    static var version2: String { return "v2/" }

    // MARK: - URLRequestConvertible
    public func asURLRequest() throws -> URLRequest {
        let url = APIRouter.baseURLWithVersion
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        switch self {
        case .authLogin, .refreshToken, .userRegister:
         break
        default:
            if let userToken = FEUserSession.getSession()?.accessToken {
                let token = "bearer\(userToken)"
                urlRequest.setValue(token, forHTTPHeaderField: HTTPHeaderField.authentication.rawValue)
            }
        }
        
        urlRequest.url = url.appendingPathComponent(path)
        //HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        //Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        
        // Parameters
        switch parameters {
        case .requestBody(let request):
            let params = try request.convertToDictionary()
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options: [])
        case .requestBodyWithDictionary(let dictionary):
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: dictionary, options: [])
        case .urlReplace(let replacements):
            var paramPath = path
            paramPath.replaceURLParameters(withParameters: replacements)
            urlRequest.url = url.appendingPathComponent(paramPath)
        case .none:
            break
        }
        return urlRequest
    }
}
