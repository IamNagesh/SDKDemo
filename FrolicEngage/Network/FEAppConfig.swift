//
//  FEAppConfig.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Alamofire

struct FEAppConfig {
    
    var appConfigMode: FEAppConfigurationMode = .debug
    static var shared = FEAppConfig(appConfigMode: .debug)
    
    init(appConfigMode: FEAppConfigurationMode) {
        self.appConfigMode = appConfigMode
    }
    var hostName: String {
        switch appConfigMode {
            case .debug: return "engage.frolicz0.de"
            case .uat: return "engage.frolicz0.de"
            case .release: return "engage.frolic.live"
        }
    }
    private var apiHostName: String { return "\(hostName)" }
    
    var apiDomainUrl: String { return "https://\(apiHostName)" }
    var apiBaseUrl: URL { URL(string: "\(apiDomainUrl)/service/application/app")! }
   
    // User Subscription
    var getUserSubscriptionDetail = "subscriptions"
    var getSubscriptionFeature = "subscriptions/:subscriptionId/features"
    var getUserSubscriptionFeature = "subscriptions/features"
    var renewUserSubscription = "subscriptions/renew"
    var upgradeUserSubscriptionPlan = "subscriptions/upgrade"


    // USer Detail
    var userDetail = "users"
    var userRegister = "users/register"
    var getUserAccesToken = "auth/token"
    var refreshAccesToken = "auth/refresh"

// User Activity
    var getuserActivityDetail = "activities/:activityCode"
    var publishuserActivity = "activities"
    var getUserAllPreviousActivity = "activities"

        
    var payload = "payload"
}

protocol APIConfiguration: URLRequestConvertible {
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: RequestParams { get }
}

enum FEAppConfigurationMode: Codable {
    case debug
    case uat
    case release
}


enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
    case string = "String"
    case platform = "X-Platform"
    case clientVersion = "X-Client-Version"
}

enum ContentType: String {
    case json = "Application/json"
    case formEncode = "application/x-www-form-urlencoded"
    case pdf = "application/pdf"
}

enum RequestParams {
     case requestBody(_:Codable)
    case requestBodyWithDictionary(_: [String: Any])
    case urlReplace(_:[URLReplaceIdentifier : String])
    case none
}

public enum URLReplaceIdentifier: String {
    case userId = ":userId"
    case subscriptionId = ":subscriptionId"
   
}
public typealias FLServiceTaskCompletionHandler = (_ response: Any?, _ error: Any?) -> Void
public typealias FLServiceSuccessBlock = ()->()
public typealias FLServiceFailureBlock = (_ errorMessage: Any?)->()
public typealias FLServiceNetworkIssueBlock = ()->()
