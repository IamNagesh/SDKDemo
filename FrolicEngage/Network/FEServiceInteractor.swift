//
//  FEServiceInteractor.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Foundation
import Alamofire

class FEServiceInteractor: FLServiceInteractorProtocol {
    
   // var baseURLWithVersion: URL { APIRouter.baseURLWithVersion }
    
    
    func request(forAPI router: APIRouter, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        AF.request(router).responseData { response in
            if response.response?.statusCode == 401 {
                self.getRefreshToke(forAPI: router, andCompletionHandler: completion)
            }  else {
                switch response.result {
                    case .success(let data):
                        do {
                            let asJSON = try JSONSerialization.jsonObject(with: data)
                            if let httpStatusCode = response.response?.statusCode,
                               (200..<300).contains(httpStatusCode) {
                                completion(asJSON, nil)
                            } else {
                                completion(nil, asJSON)
                            }
                            // Handle as previously success
                        } catch {
                            completion(nil, error.localizedDescription)
                            print("Error while decoding response: \(error) from: \(String(data: data, encoding: .utf8) ?? "")")
                            break
                        }
                    case .failure(let error):
                    print("Error while decoding response: \(error)")
                    completion(nil, error.localizedDescription)

                        break
                        // Handle as previously error
                    }
            }

        }
    }
    
    
    private func getRefreshToke(forAPI router: APIRouter, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        let session = FEUserSession.getSession()
        let request = FERefreshTokenRequest(token: session?.refreshToken)
        self.request(forAPI: .refreshToken(request)) { response, error in
            if let response = response as? [String: Any], let tokenDict = response[FEAppConfig.shared.payload] {
                FEUserSession.saveLoginSession(session: tokenDict)
            }
                self.request(forAPI: router, andCompletionHandler: completion)
            
        }
    }
}
