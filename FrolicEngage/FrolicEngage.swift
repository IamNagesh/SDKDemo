//
//  FrolicEngage.swift
//  FrolicEngage
//
//  Created by Nageshwar Agrahari on 11/10/23.
//

import Foundation
import Foundation
public final class FrolicEngage {
    
    //MARK: Initialisation
     private var interactor: FLServiceInteractorProtocol
    
     init(interactor: FLServiceInteractorProtocol){
        self.interactor = interactor
    }
    private var apiKey: String = ""
   public static let shared = FrolicEngage(interactor: FEServiceInteractor())

    
    public func configureSDK(apiKey: String) {
        self.apiKey = apiKey
    }
    
    public func verifyUser(userId: String, onSuccess:@escaping (FLServiceSuccessBlock), onFailure:@escaping FLServiceFailureBlock) {
        interactor.request(forAPI: .authLogin(apiKey, userId)) { response, error in
            if let response = response as? [String: Any], let tokenDict = response[FEAppConfig.shared.payload] {
               FEUserSession.saveLoginSession(session: tokenDict)
               onSuccess()
           } else {
               onFailure(error)
           }
        }
    }
    
    public func createUser(name: String, userId: String, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .userRegister(name, userId, apiKey: apiKey), andCompletionHandler: completion)
    }
    
    public func getUserDetails(completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .userDetail, andCompletionHandler: completion)
        }
    
    public func updateUserDetails(name: String? = nil, email: String? = nil, phone: String, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .updateUserDetail(name, email, phone: phone), andCompletionHandler: completion)

    }
    
    public func getSubscriptionFeature(subId: String, andCompletionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        let request: [URLReplaceIdentifier: String] = [.subscriptionId: subId]
        interactor.request(forAPI: .subscriptionFeature(request), andCompletionHandler: completion)
    }

    public func getUserSubscriptionPlan(completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .userSubscriptionFeature, andCompletionHandler: completion)
    }
    
    public func getUserSubscriptionFeatures(completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .userSubscriptionDetail, andCompletionHandler: completion)
    }
    
    public func renewSubscription(completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .renewSubscription, andCompletionHandler: completion)
    }
    
    public func upgradeSubscription(subscriptionId: String, completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .upgradePlan(subscriptionId), andCompletionHandler: completion)
    }
    
    public func publishUserActivityEvent(activityCode: String, meta: [String: Any] = [:], completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .publishUserActivity(activityCode, meta: meta), andCompletionHandler: completion)
    }

    public func getUserActivitiesList(subscriptionId: String, completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .getUserAllPreviousActivity, andCompletionHandler: completion)
    }
    
    public func getUserActivityDetails(subscriptionId: String, completionHandler completion: @escaping FLServiceTaskCompletionHandler) {
        interactor.request(forAPI: .userActivityDetail, andCompletionHandler: completion)
    }
}

